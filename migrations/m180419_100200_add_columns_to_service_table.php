<?php

use yii\db\Migration;

/**
 * Class m180419_100200_add_columns_to_service_table
 */
class m180419_100200_add_columns_to_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('service', 'fix_reward_sum', $this->integer());
        $this->addColumn('service', 'is_reward_fixed', $this->integer()->defaultValue(\app\models\Service::REWARD_FIXED_DISABLED));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('service', 'fix_reward_sum');
        $this->dropColumn('service', 'is_reward_fixed');
    }

}
