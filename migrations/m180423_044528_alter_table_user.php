<?php

use yii\db\Migration;
use \app\models\User;

/**
 * Class m180423_044528_alter_table_user
 */
class m180423_044528_alter_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'phone_view_status', $this->integer(1)->defaultValue(User::PHONE_DISABLED));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'phone_view_status');
    }
}
