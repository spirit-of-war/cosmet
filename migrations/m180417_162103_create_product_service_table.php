<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_service`.
 */
class m180417_162103_create_product_service_table extends Migration
{
    private $table = 'product_service';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull()
        ]);
        $this->createIndex('m_index_product_service', $this->table, 'product_id, service_id', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('m_index_product_service', $this->table);
        $this->dropTable('product_service');
    }
}
