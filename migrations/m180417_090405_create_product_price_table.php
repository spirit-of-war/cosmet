<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_price`.
 */
class m180417_090405_create_product_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_price', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_price');
    }
}
