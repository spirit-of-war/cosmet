<?php

use yii\db\Migration;

/**
 * Handles the creation of table `visit_service`.
 */
class m180415_172514_create_visit_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('visit_service', [
            'id' => $this->primaryKey(),
            'visit_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('visit_service');
    }
}
