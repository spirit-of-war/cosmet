<?php

use yii\db\Migration;

/**
 * Class m180418_060956_add_index_to_visit_service_table
 */
class m180418_060956_add_index_to_visit_service_table extends Migration
{
    private $table = 'visit_service';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('m_index_visit_service', $this->table, 'visit_id, service_id', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('m_index_visit_service', $this->table);
    }

}
