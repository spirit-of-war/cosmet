<?php

use yii\db\Migration;

/**
 * Handles adding reward_percent to table `service`.
 */
class m180418_071703_add_reward_percent_column_to_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('service', 'reward_percent', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('service', 'reward_percent');
    }
}
