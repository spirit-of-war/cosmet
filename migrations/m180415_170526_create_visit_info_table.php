<?php

use yii\db\Migration;

/**
 * Handles the creation of table `visit_info`.
 */
class m180415_170526_create_visit_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('visit_info', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'date' => $this->integer()->notNull(),
            'description' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('visit_info');
    }
}
