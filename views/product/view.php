<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукция', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить данную продукцию?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'description:ntext',
            [
                'attribute' => 'volume',
                'format' => 'raw',
                'value' => function($model) {
                    if(!$model->volume) return null;
                    return $model->volume . ' мл';
                }
            ],
            [
                'attribute' => 'actualPrice',
                'format' => 'raw',
                'value' => function($model) {
                    if(!$model->actualPrice) return null;
                    return $model->actualPrice . ' р';
                }
            ],
            [
                'attribute'=>'created_at',
                'format'=>['date', 'php:d M, Y'],
            ],
            [
                'attribute'=>'updated_at',
                'format'=>['date', 'php:d M, Y'],
            ],
        ],
    ]) ?>

</div>
