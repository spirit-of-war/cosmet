<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукция';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить продукцию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a(
                        $model->name,
                        ['/product/'.$model->id],
                        ['class' => 'name-href']
                    );
                }
            ],
            'description:ntext',
            [
                'attribute' => 'volume',
                'format' => 'raw',
                'value' => function($model) {
                    if(!$model->volume) return null;
                    return $model->volume . ' мл';
                }
            ],
            [
                'attribute' => 'actualPrice',
                'format' => 'raw',
                'value' => function($model) {
                    if(!$model->actualPrice) return null;
                    return $model->actualPrice . ' р';
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
