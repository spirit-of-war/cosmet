<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Service */
/* @var $form yii\widgets\ActiveForm */

$irId = Html::getInputId($model, 'is_reward_fixed');
$rpId = Html::getInputId($model, 'reward_percent');
$frId = Html::getInputId($model, 'fix_reward_sum');

$model->reward_percent = $model->reward_percent ?: $model::DEFAULT_REWARD_PERCENT;
?>

<div class="service-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'is_reward_fixed')->checkbox() ?>

    <div class="display-parent <?= $model->is_reward_fixed ? 'display-none' : ''?>">
    <?= $form->field($model, 'reward_percent')
        ->textInput(['disabled' => $model->is_reward_fixed ? true : false]) ?>
    </div>
    <div class="display-parent <?= $model->is_reward_fixed ? '' : 'display-none'?>">
    <?= $form->field($model, 'fix_reward_sum')
        ->textInput(['disabled' => $model->is_reward_fixed ? false : true]) ?>
    </div>
    <?= Html::activeHiddenInput($model, "user_id", ['value' => Yii::$app->user->id]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<<JS
$(".service-form #{$irId}").on("change", function (e) {
    if($(this).is(":checked")) {
        $(".service-form #{$frId}").prop('disabled', false).parents('.display-parent').removeClass('display-none');
        $(".service-form #{$rpId}").prop('disabled', true).parents('.display-parent').addClass('display-none');
    } else {
        $(".service-form #{$rpId}").prop('disabled', false).parents('.display-parent').removeClass('display-none');
        $(".service-form #{$frId}").prop('disabled', true).parents('.display-parent').addClass('display-none');
    }
});

JS;
$this->registerJs($js);