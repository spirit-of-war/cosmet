<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Service */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="service-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить данную услугу?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
        if($model->is_reward_fixed == $model::REWARD_FIXED_DISABLED) {
            $rewardArrInfo = [
                'attribute'=>'reward_percent',
                'value'=>function ($model) {
                    return $model->reward_percent . ' %';
                },
                'format'=>'raw',
            ];
        } else {
            $rewardArrInfo = [
                'attribute'=>'fix_reward_sum',
                'value'=>function ($model) {
                    return $model->fix_reward_sum . ' р';
                },
                'format'=>'raw',
            ];
        }
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'description:ntext',
            [
                'attribute'=>'price',
                'value'=>function ($model) {
                    return $model->price . ' р.';
                },
                'format'=>'raw',
            ],
//            [
//                'attribute'=>'productCost',
//                'value'=>function ($model) {
//                    return $model->productCost . ' р.';
//                },
//                'format'=>'raw',
//            ],
            $rewardArrInfo,
            [
                'attribute'=>'profit',
                'value'=>function ($model) {
                    return $model->profit . ' р.';
                },
                'format'=>'raw',
            ],
            [
                'attribute'=>'created_at',
                'format'=>['date', 'php:d M, Y'],
            ],
            [
                'attribute'=>'updated_at',
                'format'=>['date', 'php:d M, Y'],
            ],
        ],
    ]) ?>

<!--    --><?php //ActiveForm::begin(); ?>
<!---->
<!--        --><?//= Html::tag('h1', 'Продукция');?>
<!--        <div class="form-group">-->
<!--        --><?//= \kartik\select2\Select2::widget([
//            'name' => 'product_id',
//            'value' => '',
//            'data' => $productsData,
//            'options' => ['placeholder' => 'Выберите продукцию...'],
//        ]);?>
<!--        </div>-->
<!--        <div class="form-group">-->
<!--        --><?//= Html::submitButton('Добавить выбранную', ['class' => 'btn btn-warning']) ?>
<!--        </div>-->
<!--    --><?php //ActiveForm::end(); ?>
<!---->
<!--    --><?//= \kartik\grid\GridView::widget([
//        'dataProvider' => $model->getProductsDataProvider(),
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            [
//                'attribute' => 'name',
//                'format' => 'raw',
//                'label' => 'Имя',
//                'value' => function($model) {
//                    return Html::a(
//                        $model['name'],
//                        ['/product/'.$model['id']],
//                        ['class' => 'name-href']
//                    );
//                }
//            ],
//            [
//                'attribute' => 'volume',
//                'format' => 'raw',
//                'label' => 'Объем',
//                'value' => function($model) {
//                    if(!$model['volume']) return null;
//                    return $model['volume'] . ' мл';
//                }
//            ],
//            [
//                'attribute' => 'actualPrice',
//                'format' => 'raw',
//                'label' => 'Цена',
//                'value' => function($model) {
//                    if(!$model['actualPrice']) return null;
//                    return $model['actualPrice'] . ' р';
//                }
//            ],
//            [
//                'attribute' => 'id',
//                'label' => '',
//                'format' => 'raw',
//                'value' => function($m) use ($model){
//                    return Html::a(
//                        'Убрать',
//                        [$model['id'].'?remove_product_id='.$m['id']],
//                        ['class' => 'name-href']
//                    );
//                }
//            ],
//        ],
//    ]); ?>
</div>
