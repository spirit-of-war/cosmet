<?php
use \yiister\gentelella\widgets\Panel;
use \yiister\gentelella\widgets\StatsTile;
use \kartik\date\DatePicker;
use \yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $dateFrom
 * @var $dateTo
 * @var $profit
 * @var $markup
 * @var $visitInfo
 * @var $forDateProfit
 * @var $visitInfo
 */
$this->title = 'Главная';

$this->params['breadcrumbs'][] = '';
?>
<div class="row">
    <?php Panel::begin(['header' => 'Статистика',]) ?>
    <div class="statistic-date-block">
        <span>Период:</span>
        <span class="period">
            <span class="js-date-from"><?= $dateFrom?></span> - <span class="js-date-to"><?= $dateTo?></span>
        </span>
    </div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="calendar-block">
        <div class="inline-block">
            <label for="date_from">
                от
            </label>
        </div>
        <div class="inline-block">
            <?= DatePicker::widget([
                'name' => 'date_from',
                'type' => DatePicker::TYPE_BUTTON,
                'value' => $dateFrom,
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'todayBtn' => true,
                    'autoclose'=>true,
                    'format'    => 'dd-mm-yyyy',
                ],
                'options' => [
                    'onchange'=>"$('.js-date-from').text(this.value);"
                ]

            ]);
            ?>
        </div>
        <div class="inline-block">
            <label for="date_to">
                до
            </label>
        </div>
        <div class="inline-block">
            <?= DatePicker::widget([
                'name' => 'date_to',
                'type' => DatePicker::TYPE_BUTTON,
                'value' => $dateTo,
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'todayBtn' => true,
                    'autoclose'=>true,
                    'format'    => 'dd-mm-yyyy',
                ],
                'options' => [
                    'onchange'=>"$('.js-date-to').text(this.value);"
                ]
            ]);
            ?>
        </div>
        <div class="inline-block" style="margin-left: 14px;">
            <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <div class="col-xs-12 col-md-3">
        <?=
        StatsTile::widget(
            [
                'icon' => 'rouble',
                'header' => 'Доход',
                'text' => 'Ваш заработок',
                'number' => $profit.' р',
            ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?=
        StatsTile::widget(
            [
                'icon' => 'money',
                'header' => 'Общая выручка',
                'text' => 'Всего выполнено процедур на сумму',
                'number' => $markup.' р',
            ]
        )
        ?>
    </div>
    <div class="col-xs-12 col-md-3">
        <?=
        StatsTile::widget(
            [
                'icon' => 'users',
                'header' => 'Клиентов',
                'text' => 'Кол-во посещений',
                'number' => count($visitInfo),
            ]
        )
        ?>
    </div>
    <?if($forDateProfit) { ?>
    <div class="col-xs-12 col-md-12 chart-cont">
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load("current", {packages:["corechart"]});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Доход', 'В рублях'],
                    [
                        <? foreach($forDateProfit as $fDate => $fProfit) {
                            echo json_encode($fDate) . ',' . json_encode($fProfit);
                        } ?>
                    ]
                ]);

                var options = {
                    title: 'Доход по датам (руб)',

                    legend: 'none',
                    pieSliceText: 'label',
                    width: 250,
                    height: 250,
                    titleTextStyle: {
                        color: '#73879C',
                        fontSize: 13
                    }
                };

                var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                chart.draw(data, options);
            }
        </script>
        <div id="piechart"></div>
    </div>
    <? } ?>
    <?php Panel::end() ?>

</div>