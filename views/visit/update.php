<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VisitInfo */
/* @var $clientsData array */

$this->title = 'Редактирование: ';
$this->params['breadcrumbs'][] = ['label' => 'Посещения', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="visit-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'clientsData' => $clientsData
    ]) ?>

</div>
