<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VisitInfo */
/* @var $clientsData array */

$this->title = 'Создание посещения';
$this->params['breadcrumbs'][] = ['label' => 'Посещения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visit-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'clientsData' => $clientsData,
    ]) ?>

</div>
