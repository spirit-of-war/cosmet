<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \app\models\VisitInfo;
use \yii\widgets\ActiveForm;
use yii\grid\GridView;
use \kartik\select2\Select2;
use \yiister\gentelella\widgets\Panel;


/* @var $this yii\web\View */
/* @var $model app\models\VisitInfo */

$this->title = $model->clientFullName() . ' от ' . date('d-m-Y',$model->date);
$this->params['breadcrumbs'][] = ['label' => 'Посещения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visit-info-view">
    <?if (isset($visitStatusUpdate)):?>
        <div class="alert <?= $visitStatusUpdate['is_error'] ? 'alert-danger': 'alert-success'?> alert-dismissable status-message">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?= $visitStatusUpdate['message']?>
        </div>
    <?endif?>

<!--    <h2>--><?//= Html::encode($this->title) ?><!--</h2>-->

    <p style="margin-top:10px;">
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить данное посещение?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'client_id',
                'format' => 'raw',
                'value' => function(VisitInfo $model){
                    return $model->clientFullName();
                }
            ],
            [
                'attribute'=>'date',
                'format'=>['date', 'php:d M, Y'],
            ],
            [
                'attribute'=>'price',
                'value'=>function ($model) {
                    return $model->price . ' р.';
                },
                'format'=>'raw',
            ],
//            [
//                'attribute'=>'serviceCost',
//                'value'=>function ($model) {
//                    return $model->serviceCost . ' р.';
//                },
//                'format'=>'raw',
//            ],
//            [
//                'attribute'=>'markup',
//                'value'=>function ($model) {
//                    return $model->markup . ' р.';
//                },
//                'format'=>'raw',
//            ],
            [
                'attribute'=>'profit',
                'value'=>function ($model) {
                    return $model->profit . ' р.';
                },
                'format'=>'raw',
            ],
            'description:ntext',
        ],
    ]) ?>

    <?php Panel::begin(['header' => 'Услуги']) ?>
        <?php ActiveForm::begin(); ?>
            <div class="form-group">
                <?= Select2::widget([
                    'name' => 'service_id',
                    'value' => '',
                    'data' => $serviceData,
                    'options' => ['placeholder' => 'Выберите услугу...'],
                ]);?>
            </div>
            <div class="form-group add-button-cont">
                <?= Html::submitButton('Добавить выбранную', ['class' => 'btn btn-warning add-service-btn']) ?>
            </div>
        <?php ActiveForm::end(); ?>
        <div>
        <?= GridView::widget([
            'dataProvider' => $model->getServicesDataProvider(),
            'filterModel' => null,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'label' => 'Имя',
                    'value' => function($model) {
                        return Html::a(
                            $model['name'],
                            ['/service/'.$model['id']],
                            ['class' => 'name-href']
                        );
                    }
                ],
                [
                    'attribute'=>'price',
                    'value'=>function ($model) {
                        return $model->price . ' р.';
                    },
                    'format'=>'raw',
                ],
    //            [
    //                'attribute'=>'productCost',
    //                'value'=>function ($model) {
    //                    return $model->productCost . ' р.';
    //                },
    //                'format'=>'raw',
    //            ],
                [
                    'attribute' => 'id',
                    'label' => '',
                    'format' => 'raw',
                    'value' => function($m) use ($model){
                        return Html::a(
                            'Убрать',
                            ['view?id='.$model['id'].'&remove_service_id='.$m['id']],
                            ['class' => 'name-href']
                        );
                    }
                ],
            ],
        ]); ?>
        </div>
    <?php Panel::end() ?>
</div>
