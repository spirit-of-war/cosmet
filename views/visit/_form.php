<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\date\DatePicker;
use \kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\VisitInfo */
/* @var $form yii\widgets\ActiveForm */
/* @var $clientsData array */

?>

<div class="visit-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'client_id')
        ->widget(Select2::classname(), [
            'data' => $clientsData,
            'options' => ['placeholder' => 'Поиск клиента'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'options' => [
            'value' => $model->date ? Yii::$app->formatter->asDate($model->date) : '',
            'placeholder' => 'Введите дату посещения'
        ],
        'readonly' => true,
        'pluginOptions' => [
            'todayHighlight' => true,
            'todayBtn' => true,
            'autoclose'=>true,
            'format'    => 'dd-mm-yyyy',
        ]
    ]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= Html::activeHiddenInput($model, "user_id", ['value' => Yii::$app->user->id]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
