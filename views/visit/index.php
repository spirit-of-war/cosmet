<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \app\models\VisitInfo;
use \kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VisitInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Посещения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visit-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить посещение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
//        'rowOptions' => function(VisitInfo $model){
//            return (date('d-m-Y',$model->date) === date('d-m-Y')) ?
//                ['style' => 'background-color: #f1ede0;'] : [];
//        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'full_name',
                'format' => 'raw',
                'label' => 'ФИО',
                'value' => function(VisitInfo $model) {
                    $result = Html::a(
                        $model->clientFullName(),
                        ['/visit-info/view?id='.$model->id],
                        ['class' => 'name-href']
                    );
                    if(date('d-m-Y',$model->date) === date('d-m-Y')) {
                        $result = '<small class="label-success label pull-right"> С </small> ' . $result;
                    }
                    return $result;
                }
            ],
            [
                'attribute'=>'date',
                'format'=>['date', 'php:d M, Y'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'find_date',
                    'readonly' => true,
                    'type' => DatePicker::TYPE_BUTTON,
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose'=>true,
                        'format'    => 'dd-mm-yyyy',
                    ]
                ])
            ],
            [
                'attribute' => 'profit',
                'format' => 'raw',
                'value' => function(VisitInfo $model) {
                    return $model->profit . 'р';
                }
            ],
//            [
//                'class' => 'yii\grid\ActionColumn',
//            ],
        ],
    ]); ?>
</div>
