<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use \yiister\gentelella\widgets\Menu;
use \yii\widgets\Breadcrumbs;

$bundle = yiister\gentelella\assets\Asset::register($this);
\yii\web\JqueryAsset::register($this);
?>
<?php if (class_exists('yii\debug\Module')) {
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
}?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="/css/site.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>">
<?php $this->beginBody(); ?>
<div class="container body">

    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title"><i class="logo-cosmet"></i> <span>Мой косметолог</span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="http://placehold.it/128x128" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Вы зашли как</span>
                        <h2><?= Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->username ?></h2>
                    </div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <?=
                        Menu::widget(
                            [
                                "items" => [
                                    [
                                        "label" => "Главная",
                                        "url" => "/",
                                        "icon" => "home",
                                        'active' => \Yii::$app->controller->id == 'info'
                                    ],
                                    [
                                        "label" => "Клиенты",
                                        "url" => "/client/",
                                        "icon" => "group",
                                        'active' => \Yii::$app->controller->id == 'client'
//                                        "items" => [
//                                            [
//                                                "label" => "Список",
//                                                "url" => "/client/",
//                                            ],
//                                            [
//                                                "label" => "Создать",
//                                                "url" => "/client/create",
//                                            ],
//                                        ],
                                    ],
//                                    [
//                                        "label" => "Продукция",
//                                        "url" => "#",
//                                        "icon" => "table",
//                                        "items" => [
//                                            [
//                                                "label" => "Список",
//                                                "url" => "/product/",
//                                            ],
//                                            [
//                                                "label" => "Создать",
//                                                "url" => "/product/create",
//                                            ],
//                                        ],
//                                    ],
                                    [
                                        "label" => "Услуги",
                                        "url" => "/service/",
                                        "icon" => "medkit",
                                        'active' => \Yii::$app->controller->id == 'service'
//                                        "items" => [
//                                            [
//                                                "label" => "Список",
//                                                "url" => "/service/",
//                                            ],
//                                            [
//                                                "label" => "Создать",
//                                                "url" => "/service/create",
//                                            ],
//                                        ],
                                    ],
                                    [
                                        "label" => "Посещения",
                                        "url" => "/visit-info/",
                                        "icon" => "calendar",
                                        'active' => \Yii::$app->controller->id == 'visit-info'
//                                        "items" => [
//                                            [
//                                                "label" => "Список",
//                                                "url" => "/visit-info/",
//                                            ],
//                                            [
//                                                "label" => "Создать",
//                                                "url" => "/visit-info/create",
//                                            ],
//                                        ],
                                    ],
                                ],
                            ]
                        )
                        ?>
                    </div>

                </div>
                <!-- /sidebar menu -->

            </div>
        </div>

        <!-- top navigation -->


            <div class="nav_menu scroll-fixed">
                <div class="top_nav">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
<!--                                    <img src="http://placehold.it/128x128" alt="">-->
                                    <?= Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->username ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li>
                                        <?= Html::a('<i class="fa fa-sign-out pull-right"></i> Выйти',
                                            '/site/logout', [
                                                'data' => [
                                                    'method' => 'post',
                                                ],
                                            ]) ?>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>


        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col cosmet-main-cont" role="main">
            <?php if (isset($this->params['h1'])): ?>
                <div class="page-title">
                    <div class="title_left">
                        <h1><?= $this->params['h1'] ?></h1>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]);?>
            <div class="clearfix"></div>

            <?= $content ?>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            <div class="pull-right my-cosmet">
                Система CRM Мой Косметолог<br />
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>

</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
<script>
    var windowAppSize = {
        dWidth: $(document).width(),
        bWidth: $('body').width(),
        init: function(){
            if(this.dWidth > this.bWidth) {
                $('body').width(this.dWidth + 30)
            }
        }
    };
    windowAppSize.init();
    <?if (Yii::$app->user->id === 1 || Yii::$app->user->id === 2) {
        // Это короче злой сюрприз-функционал. Жмякаем 4 раза на "мой косметолог" ->
        // врубаем или вырубаем телефоны на всем сайте
        ?>
        $(document).ready(function(){
            var timer,
                message = <?= json_encode(Yii::$app->user->identity->isPhoneEnabled() ? 'Скрыть телефоны?' : 'Отобразить телефоны?') ?>,
                clickCount = 0;
            $('footer .my-cosmet').on('click',function(){
                if(!timer) {
                    timer = setTimeout(function(){
                        clickCount = 0;
                        timer = null;
                    }, 2000);
                }
                clickCount++;
                if(clickCount>4) {
                    var c = confirm(message);
                    if(c) {
                        location.href = '/info/turn/';
                    }
                }
            })
        });
    <? }?>
</script>