<?php
use \yii\bootstrap\ActiveForm;
use \yii\bootstrap\Html;

$this->title = 'Вход в личный кабинет';
?>

<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">

                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <h1>Личный кабинет</h1>
                <div>
                    <?= $form->field($model, 'username')->textInput([
                        'autofocus' => true, 'placeholder' => 'логин'])->label(false) ?>
                </div>
                <div>
                    <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'пароль'])
                        ->label(false) ?>
                </div>
                <div>
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>

                    <?= Html::submitButton('Войти', ['class' => 'btn btn-default submit', 'name' => 'login-button']) ?>

                    <a class="reset_pass" href="#">Забыли пароль?</a>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <p class="change_link">Новенький?
                        <a href="/signup" class="to_register"> Зарегистрироваться </a>
                    </p>

                    <div class="clearfix"></div>
                    <br />

                    <div>
                        <h1><i class="fa fa-paw"></i> Мой Косметолог</h1>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

            </section>
        </div>
    </div>
</div>