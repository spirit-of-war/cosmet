<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yiister\gentelella\widgets\Panel;
use yii\grid\GridView;
use \app\models\VisitInfo;
use \yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить данного клиента?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?
    $detailViewData = [
        'model' => $model,
        'attributes' => [
            'name',
            'last_name',
            'middle_name',
        ]
    ];
    if(Yii::$app->user->identity->isPhoneEnabled()) {
        $detailViewData['attributes'][] = 'phone';
    }
    $detailViewData['attributes'][] = [
        'attribute'=>'date_of_birth',
        'format'=>['date', 'php:d M, Y'],
    ];
    $detailViewData['attributes'][] = [
        'attribute'=>'created_at',
        'format'=>['date', 'php:d M, Y'],
    ];
    $detailViewData['attributes'][] = [
        'attribute'=>'updated_at',
        'format'=>['date', 'php:d M, Y'],
    ];
    ?>
    <?= DetailView::widget($detailViewData) ?>

    <div class="row">
        <?php Panel::begin(['header' => 'Посещения',]) ?>

        <?= Html::a(
            'Создать клиенту',
            ['/visit-info/create?client_id='.$model->id],
            ['class' => 'btn btn-warning add-service-client-btn']
        );?>
        <?= GridView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->getVisits(),
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'date' => SORT_DESC
                    ]
                ],

            ]),
//            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute'=>'date',
                    'format'=>['date', 'php:d M, Y'],
                ],
                [
                    'attribute' => 'markup',
                    'format' => 'raw',
                    'value' => function($model) {
                        if(!$model->markup) return null;
                        return Html::a(
                            $model->markup . ' р.',
                            ['/visit-info/view?id='.$model->id],
                            ['class' => 'name-href']
                        );
                    }
                ],
                [
                    'attribute' => 'profit',
                    'format' => 'raw',
                    'value' => function($model) {
                        if(!$model->profit) return null;
                        return Html::a(
                            $model->profit . ' р.',
                            ['/visit-info/view?id='.$model->id],
                            ['class' => 'name-href']
                        );
                    }
                ],
            ],
        ]); ?>
        <?php Panel::end() ?>
    </div>
</div>
