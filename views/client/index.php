<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;

//var_dump(Yii::$app->user->identity->isPhoneEnabled());
?>
<div class="client-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить клиента', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <? $gridViewData = [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'full_name',
                'format' => 'raw',
                'label' => 'ФИО',
                'value' => function($model) {
                    return Html::a(
                        $model->last_name . ' ' . $model->name,
                        ['/client/'.$model->id],
                        ['class' => 'name-href']
                    );
                }
            ],
        ]
    ];
    if (Yii::$app->user->identity->isPhoneEnabled()) {
        $gridViewData['columns'][] = [
            'attribute' => 'phone',
            'format' => 'raw',
            'value' => function($model) {
                return Html::a(
                    $model->phone,
                    ['/client/'.$model->id],
                    ['class' => 'name-href']
                );
            }
        ];
    }
    ?>
    <?= GridView::widget($gridViewData); ?>
</div>
