<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

    <? if(Yii::$app->user->identity->isPhoneEnabled()) {
        echo $form->field($model, 'phone')->textInput(['maxlength' => true]);
    } ?>


    <?= $form->field($model, 'date_of_birth')->widget(DatePicker::classname(), [
        'options' => [
            'value' => $model->date_of_birth ? Yii::$app->formatter->asDate($model->date_of_birth) : '',
            'placeholder' => 'Введите дату рождения'
        ],
        'readonly' => true,
        'pluginOptions' => [
            'todayHighlight' => true,
            'autoclose'=>true,
            'format'    => 'dd-mm-yyyy',
        ]
    ]) ?>

    <?= Html::activeHiddenInput($model, "user_id", ['value' => Yii::$app->user->id]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
