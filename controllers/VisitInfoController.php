<?php

namespace app\controllers;

use app\models\Client;
use app\models\Service;
use Yii;
use app\models\VisitInfo;
use app\models\VisitInfoSearch;
use yii\db\IntegrityException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VisitInfoController implements the CRUD actions for VisitInfo model.
 */
class VisitInfoController extends BaseController
{
    public $clientsData;

    public function getViewPath()
    {
        return '@app/views/visit';
    }

    public function init()
    {
        parent::init();
        $this->clientsData = Client::getAvailableClients();
    }

    /**
     * Lists all VisitInfo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VisitInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'clientsData' => $this->clientsData
        ]);
    }

    /**
     * Displays a single VisitInfo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $session = Yii::$app->session;
        $visitModel = $this->findModel($id);

        if($service_id = Yii::$app->request->post('service_id')) {
            $service = Service::findOne($service_id);
            try {
                $visitModel->link('services', $service);
                $session->set('visit_status_update',[
                    'is_error' => false,
                    'message' => 'Услуга добавлена успешно'
                ]);
            } catch(IntegrityException $e) {
                $session->set('visit_status_update',[
                    'is_error' => true,
                    'message' => 'Ошибка! Выбранная услуга уже добавлена'
                ]);
            }

            return $this->redirect(['view', 'id' => $visitModel->id]);
        }

        if($remove_service_id = Yii::$app->request->get('remove_service_id')) {
            $service = Service::findOne($remove_service_id);
            $visitModel->unlink('services', $service, true);
            $session->set('visit_status_update',[
                'is_error' => false,
                'message' => 'Услуга успешно удалена из посещения'
            ]);
            return $this->redirect(['view', 'id' => $visitModel->id]);
        }


        if(!empty($_SESSION['visit_status_update'])){
            $visitStatusUpdate = $_SESSION['visit_status_update'];
            unset($_SESSION['visit_status_update']);
        }
        $serviceData = Service::getAvailableServices();
        return $this->render('view', [
            'model' => $visitModel,
            'clientsData' => $this->clientsData,
            'serviceData' => $serviceData,
            'visitStatusUpdate' => !empty($visitStatusUpdate) ? $visitStatusUpdate : null
        ]);
    }

    /**
     * Creates a new VisitInfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VisitInfo();

        if($clientId = Yii::$app->request->get('client_id')) {
            $model->client_id = $clientId;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'clientsData' => $this->clientsData,
        ]);
    }

    /**
     * Updates an existing VisitInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'clientsData' => $this->clientsData
        ]);
    }

    /**
     * Deletes an existing VisitInfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VisitInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VisitInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VisitInfo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
