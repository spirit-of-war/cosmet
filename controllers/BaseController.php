<?php
namespace app\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;


class BaseController extends Controller
{
    public $user_id;

    public function init()
    {
        parent::init();
        $this->user_id = \Yii::$app->user->id;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


}