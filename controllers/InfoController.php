<?php

namespace app\controllers;

use app\models\User;
use app\models\VisitInfo;
use Yii;

class InfoController extends BaseController
{
    public function actionIndex()
    {

        $dateFrom = date('d-m-Y');
        $dateTo = date('d-m-Y');
        if($post = Yii::$app->request->post()) {
            $dateFrom = $post['date_from'];
            $dateTo = $post['date_to'];
        }


        $visitInfo = VisitInfo::find()
            ->where(['user_id' => \Yii::$app->user->id])
            ->andWhere(['>=','created_at', strtotime($dateFrom)])
            ->andWhere(['<','created_at', strtotime($dateTo . "+1 day")])
            ->all();
        $profit = 0;
        $markup = 0;
        $forDateProfit = [];
        foreach($visitInfo as $info) {
            $dateIdx = date('d-m-Y',$info['created_at']);
            if(empty($forDateProfit[$dateIdx])) {
                $forDateProfit[$dateIdx] = 0;
            }
            $forDateProfit[$dateIdx] += $info->profit;
            $profit += $info->profit;
            $markup += $info->markup;
        }

        return $this->render('index', [
            'visitInfo' => $visitInfo,
            'profit' => $profit,
            'markup' => $markup,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'forDateProfit' => $forDateProfit
        ]);
    }

    public function actionTurn()
    {
        if (Yii::$app->user->id === 1 || Yii::$app->user->id === 2) {
            if(Yii::$app->user->identity->isPhoneEnabled()){
                $status = User::PHONE_DISABLED;
                $message = 'Телефоны спрятаны';
            } else {
                $status = User::PHONE_ENABLED;
                $message = 'Телефоны отображаются';
            }
            Yii::$app->db->createCommand("UPDATE user SET phone_view_status={$status} where id={$this->user_id}")->execute();
            return $this->render('turn', [
                'message' => $message
            ]);
        } else {
            return $this->redirect('/');
        }

    }

}