<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */

$activateLink = Yii::$app->urlManager->createAbsoluteUrl(['activate-user', 'key' => $user->getAuthKey()]);
$site = Yii::$app->urlManager->createAbsoluteUrl(['/']);
?>
<div class="password-reset">
    <p>Добрый день!</p>

    <p>Вы зарегистрировались в системе <?= $site ?></p>

    <p>Для подтверждения регистрации пройдите по ссылке <?= Html::a(Html::encode($activateLink), $activateLink) ?></p>
</div>
