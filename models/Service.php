<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $user_id
 * @property int $price
 * @property int $reward_percent
 * @property int $fix_reward_sum
 * @property int $is_reward_fixed
 * @property int $created_at
 * @property int $updated_at
 */
class Service extends \yii\db\ActiveRecord
{
    public $productCost = 0;
    public $markup = 0;
    public $profit = 0;

    const REWARD_FIXED_DISABLED = 0;
    const REWARD_FIXED_ENABLED = 1;

    const DEFAULT_REWARD_PERCENT = 40;

    public function afterFind()
    {
//        if($this->products) {
//            foreach($this->products as $product) {
//                $this->productCost += $product->actualPrice;
//            }
//        }
        $this->markup = $this->price - $this->productCost;
        if($this->is_reward_fixed == self::REWARD_FIXED_DISABLED) {
            $this->profit = round($this->markup * $this->reward_percent/100);
        } else {
            $this->profit = $this->fix_reward_sum;
        }

    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'user_id'], 'required'],
            [['description'], 'string'],
            [['name', 'user_id'], 'unique', 'targetAttribute' => ['name', 'user_id'], 'message' => 'Такая услуга уже существует'],
            [['user_id', 'price', 'created_at', 'updated_at', 'reward_percent', 'fix_reward_sum', 'is_reward_fixed'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['reward_percent', 'required', 'when' => function ($model) {
                return $model->is_reward_fixed == self::REWARD_FIXED_DISABLED;
            }],
            ['fix_reward_sum', 'required', 'when' => function ($model) {
                return $model->is_reward_fixed == self::REWARD_FIXED_ENABLED;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'user_id' => 'User ID',
            'price' => 'Цена',
            'productCost' => 'Стоимость продукции',
            'markup' => 'Прибыль',
            'profit' => 'Доля',
            'reward_percent' => 'Доля косметолога (%)',
            'fix_reward_sum' => 'Фиксированная доля косметолога (руб)',
            'is_reward_fixed' => 'Доля фиксирована?',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата последнего обновления',
        ];
    }

    public function getProductsDataProvider()
    {
        $query = $this->getProducts();
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'name',
                ],
            ],

        ]);
    }

    public static function getAvailableServices()
    {
        $services = self::find()->where(['user_id' => Yii::$app->user->id])
            ->orderBy('name')->asArray()->all();
        $items = ArrayHelper::map($services, 'id', 'name');
        return $items;
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable('product_service', ['service_id' => 'id']);
    }
}
