<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VisitInfo;

/**
 * VisitInfoSearch represents the model behind the search form of `app\models\VisitInfo`.
 */
class VisitInfoSearch extends VisitInfo
{
    public $full_name;
    public $find_date;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'client_id'], 'integer'],
            [['description', 'full_name', 'find_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VisitInfo::find()
            ->joinWith('client')
            ->where(['visit_info.user_id' => Yii::$app->user->id]);;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                    'id' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
        ]);

        if(Yii::$app->user->identity->isPhoneEnabled()) {
            $query->andFilterWhere(['like', 'concat(client.last_name, " ", client.name, " ", client.phone)', $this->full_name]);
        } else {
            $query->andFilterWhere(['like', 'concat(client.last_name, " ", client.name)', $this->full_name]);
        }
        $query->andFilterWhere(['like', 'description', $this->description]);

        if($this->find_date) {
            $query->andFilterWhere(['>=', 'date', strtotime($this->find_date)])
                ->andFilterWhere(['<', 'date', strtotime($this->find_date . '+1 day')]);
        }
        return $dataProvider;
    }
}
