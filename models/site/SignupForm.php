<?php
namespace app\models\site;

use Yii;
use yii\base\Exception;
use yii\base\Model;
use app\models\User;
use yii\swiftmailer\Mailer;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    protected $user;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            [['email'], 'required'],
            ['email', 'email'],
            [['email'], 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->generateAuthKey();

        $this->user = $user;
        return $user->save() ? $user : null;
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Адрес электронной почты',
            'username' => 'Имя пользователя',
        ];
    }

    public function sendMail()
    {
        if(!$this->user)
            throw new Exception('Пользователь не указан');

        $transport = Yii::$app->mailer->transport;
        $mailer    = new Mailer();
        $mailer->setTransport($transport);

        $mailer
            ->compose('registrationConfirmLink', [
                'user' => $this->user,
            ])
            ->setFrom($transport->getUsername())
            ->setTo($this->user->email)
            ->setSubject('Подтверждение регистрации в системе Мой Косметолог')
            ->send();
    }
}
