<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property double $volume
 * @property int $created_at
 * @property int $updated_at
 */
class Product extends \yii\db\ActiveRecord
{
    public $actualPrice;

    public function afterFind()
    {
        if($this->price) {
            $this->actualPrice = $this->price->price;
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'actualPrice'], 'required'],
            [['description'], 'string'],
            [['volume'], 'number'],
            [['created_at', 'updated_at', 'actualPrice'], 'integer'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'volume' => 'Объем',
            'actualPrice' => 'Цена',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата последнего обновления',
        ];
    }

    public function getPrice()
    {
        return $this->hasOne(ProductPrice::className(), ['product_id' => 'id'])
            ->orderBy('created_at DESC');
    }

    public static function getAvailableProducts()
    {
        $products = self::find()->orderBy('name')->asArray()->all();
        $items = ArrayHelper::map($products, 'id', 'name');
        return $items;
    }
}
