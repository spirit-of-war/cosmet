<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $name
 * @property string $last_name
 * @property string $middle_name
 * @property string $phone
 * @property int $user_id
 * @property int $date_of_birth
 * @property int $created_at
 * @property int $updated_at
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'last_name', 'user_id'], 'required'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'last_name', 'user_id'], 'unique', 'targetAttribute' => ['name', 'last_name', 'user_id'], 'message' => 'Такой клиент уже существует'],
            [['name', 'last_name', 'middle_name', 'phone'], 'string', 'max' => 255],
            ['date_of_birth', 'filter', 'filter' => function($value) {
                if(!$value) return null;
                return strtotime($value);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'phone' => 'Телефон',
            'user_id' => 'User ID',
            'date_of_birth' => 'Дата рождения',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата последнего обновления',
        ];
    }

    public static function getAvailableClients()
    {
        $clients = self::find()->where(['user_id' => Yii::$app->user->id])
            ->orderBy('last_name')->asArray()->all();
        $items = ArrayHelper::map($clients, 'id', function($model) {
            if(Yii::$app->user->identity->isPhoneEnabled() && $model['phone']) {
                return $model['last_name'] . ' ' . $model['name'] . ' | ' . $model['phone'];
            } else {
                return $model['last_name'] . ' ' . $model['name'];
            }

        });
        return $items;
    }

    public function getVisits()
    {
        return $this->hasMany(VisitInfo::className(),['client_id' => 'id']);
    }
}
