<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "visit_info".
 *
 * @property int $id
 * @property int $user_id
 * @property int $client_id
 * @property int $date
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 */
class VisitInfo extends \yii\db\ActiveRecord
{
    public $price = 0;
    public $serviceCost = 0;
    public $markup = 0;
    public $profit = 0;

    public function afterFind()
    {
        if($this->services) {
            foreach($this->services as $service) {
                $this->price += $service->price;
                $this->serviceCost += $service->productCost;
                $this->markup += $service->markup;
                $this->profit += $service->profit;
            }
        }
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visit_info';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'client_id', 'date'], 'required'],
            [['user_id', 'client_id', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            ['date', 'filter', 'filter' => function($value) {
                if(!$value) return null;
                return strtotime($value);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'client_id' => 'Клиент',
            'date' => 'Дата',
            'price' => 'Цена',
            'serviceCost' => 'Себестоимость',
            'markup' => 'Прибыль',
            'profit' => 'Доля',
            'description' => 'Описание',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    public function clientFullName()
    {
        if(Yii::$app->user->identity->isPhoneEnabled() && $this->client->phone) {
            return $this->client->last_name . ' ' . $this->client->name . ' | ' . $this->client->phone;
        } else {
            return $this->client->last_name . ' ' . $this->client->name;
        }

    }

    public function getServicesDataProvider()
    {
        $query = $this->getServices();
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'name',
                ],
            ],

        ]);
    }

    public function getServices()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])
            ->viaTable('visit_service', ['visit_id' => 'id']);
    }
}
